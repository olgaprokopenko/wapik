$(".nav__btn").click(function () {
    if ($(".nav__menu").is(":hidden")) {
        $(".nav__menu").slideDown("slow");
    } else {
        $(".nav__menu").hide("slow");
    }
});

var elem = document.getElementsByClassName('tabs-item');
var activeClass = function () {
    for (var i = 0; i < elem.length; i++) {
        elem[i].classList.remove('active');
        this.classList.add('active');
    }
};
for (var i = 0; i < elem.length; i++) {
    elem[i].addEventListener('click', activeClass);    
}

function change1 () {
    document.getElementById('price-1').innerText = '$199';
    document.getElementById('price-2').innerText = '$399';
    document.getElementById('price-3').innerText = '$699';
    var arrElem = document.getElementsByClassName('item-period');
    for (var j = 0; j < arrElem.length; j++) {
        arrElem[j].innerText = '/month';
    }
}
function change2 () {
    document.getElementById('price-1').innerText = '$179';
    document.getElementById('price-2').innerText = '$379';
    document.getElementById('price-3').innerText = '$679';
    var arrElem = document.getElementsByClassName('item-period');
    for (var j = 0; j < arrElem.length; j++) {
        arrElem[j].innerText = '/month';
    }
}
function change3 () {
    document.getElementById('price-1').innerText = '$250';
    document.getElementById('price-2').innerText = '$450';
    document.getElementById('price-3').innerText = '$750';
    var arrElem = document.getElementsByClassName('item-period');
    for (var j = 0; j < arrElem.length; j++) {
        arrElem[j].innerText = '/year';
    }
}




